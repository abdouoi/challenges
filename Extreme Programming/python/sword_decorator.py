import shutil


class sword_decorator(object):

    def __init__(self, f):
        print("inside myDecorator.__init__()")
        f() # Prove that function definition has completed

        # destination = open('stickman_test.txt', 'wb')
        # shutil.copyfileobj(open('sword.txt', 'rb'), destination)
        # shutil.copyfileobj(open('stickman.txt', 'rb'), destination)
        # destination.close()

        with open("sword.txt") as xh:
            with open('stickman.txt') as yh:
                with open("stickman_with_sword.txt", "w") as zh:
                    # Read first file
                    xlines = xh.readlines()
                    # Read second file
                    ylines = yh.readlines()
                    # Combine content of both lists
                    # combine = list(zip(ylines,xlines))
                    # Write to third file
                    for i in range(len(xlines)):
                        if len(xlines[i].strip()) > 0:
                            line = xlines[i] + ylines[i]
                        else:
                            line = xlines[i].rstrip() + ylines[i]

                        #if "\\\n\n" in line:
                        line.replace("_", "--")
                        #if line == "\\\n\n":
                            #line = "\\\n"
                        print(line)
                        zh.write(line)

    def __call__(self):
        print("inside myDecorator.__call__()")

@sword_decorator
def aFunction():
    print("inside aFunction()")