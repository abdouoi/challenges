from sword_decorator import sword_decorator


@sword_decorator
def stickman():
    return open('stickman.txt').read()

if __name__ == "__main__":
    print(stickman())
