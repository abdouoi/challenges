#ifndef HONDACIVIC_H_
#define HONDACIVIC_H_

#include "ReferencedObject.h"
#include "VehicleInterface.h"

struct HondaCivic {
    struct ReferencedObject *referencedObject;
    struct VehicleInterface vehicleCommands;
};

struct HondaCivic *HondaCivic_new(void);
void HondaCivic_delete(struct HondaCivic *hondaCivic);

void HondaCivic_startEngine(struct HondaCivic *hondaCivic);
void HondaCivic_stopEngine(struct HondaCivic *hondaCivic);
void HondaCivic_setSpeed(struct HondaCivic *hondaCivic, short speedInKmh);
void HondaCivic_steer(struct HondaCivic *hondaCivic, short angleInDegrees);
void HondaCivic_setFlashers(struct HondaCivic *hondaCivic, _Bool status);
unsigned int HondaCivic_getAmountOfFuelInL(struct HondaCivic *hondaCivic);

#endif
