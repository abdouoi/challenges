package cstest.java_xp_challenge;

import java.util.HashMap;
import java.util.List;

public class BloopGoopPooper {
  public HashMap<Integer, String> bloopPoopize(List<Integer> intList) {

    return intList.stream().collect(HashMap<Integer, String>::new, (hashMap, integer) ->  {
      String category = "";

      if (integer % 3 == 0) {
        category += "bloop-";
      }
      if (integer % 5 == 0) {
        category += "poop-";
      }
      if (isPrime(integer)) {
        category += "goop-";
      }

      if (!category.isEmpty()) {
        category = category.substring(0, category.length() - 1);
      }

      if (!category.isEmpty()) {
        hashMap.put(integer, category);
      }

    }, (hashMap, newHashMap) -> {});
  }

  private static boolean isPrime(int num) {
    if (num < 2)
      return false;
    if (num == 2)
      return true;
    if (num % 2 == 0)
      return false;
    for (int i = 3; i * i <= num; i += 2)
      if (num % i == 0)
        return false;
    return true;
  }
}